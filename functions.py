import requests
import os
import sqlite3

import pandas as pd
import sqlalchemy

STG_TABLE_NAME = 'STG_EXCHANGE_RATES'
DATABASE_NAME = 'exchange_rate_db'
DIR_PATH = os.path.dirname(os.path.realpath(__file__))


def exchangerates_api_dataframe(valid_api_string: str):
    """With a valid exchangerate apistring, return a cleansed dataframe
       Assumes API is called with "History_between_start_end" dates. This is
       expected due to how the API delivers the data when "multiple" dates are
       selected.
    Arguments:
        valid_api_string {str} -- Valid exchangerate apistring.
                                  Assumed that this checked before
                                  this function is called
    """
    request = requests.get(valid_api_string)
    data = request.json()
    raw_df = pd.DataFrame.from_dict(data)
    df_final = pd.DataFrame()
    for index_date, row in raw_df.iterrows():
        # Flatten the data so that it so that we are only
        # looking at the exchange rates
        df_day = pd.DataFrame(row['rates'], index=['EXCHANGE_RATE'])
        df_day = df_day.transpose()
        df_day['EFFECTIVE_DATE'] = index_date
        df_day['BASE_CURRENCY_IDNT'] = row['base']
        df_final = df_final.append(df_day)
    return df_final


def validate_exchangerate_api_string(api_string: str):
    """Using an unvalidated api string,
       check to see if valid response code is being received

    Arguments:
        api_string {str} -- Unvalidated api_string
    """
    response = requests.get(api_string)
    if response.status_code == 200:
        return True
    else:
        return False


def build_exchangerates_api_string(end_date, start_date=None,
                                   base_currency='EUR', comparisons=['All']):
    """Build a valide ExchangeRates API String

    Arguments:
        end_date str -- Date string in format of 'YYYY-MM-DD'

    Keyword Arguments:
        start_date str -- Date string in format of 'YYYY-MM-DD'
        base_currency {str} -- Type of currency that will be used for
                               comparison. This is a 3 char string
                               (default: {'EUR'})
        comparisons {list} -- [List] of Currency Symbols to filter
                              compared currencies. These symbols are
                              3 char strings. (default: {['All']})
    """
    base_url = 'https://api.exchangeratesapi.io/'
    temp_search = ''
    if start_date is None:
        temp_search = f'history?start_at={end_date}&end_at={end_date}'
    if start_date is not None:
        temp_search = f'history?start_at={start_date}&end_at={end_date}'
    if base_currency != 'EUR':
        temp_search = temp_search + f'&base={base_currency}'
    if comparisons != ['All']:
        temp_search = temp_search + f'&symbols={",".join(comparisons)}'

    return base_url + temp_search


def execute_sql_into_db(action_type: str):
    """Execute SQL file(s) based on action_type.
    Arguments:
        action_type {str} -- Defines CRUD and which folder to run SQL for
    """
    # Context manager ensures we have a DB to connect to
    with sqlite3.connect(f'{DIR_PATH}\{DATABASE_NAME}.sqlite3') as conn:
        cur = conn.cursor()
        dir = f'{DIR_PATH}\sql\{action_type}\\'
        for file in os.listdir(dir):
            with open(dir + file) as sql:
                cur.execute("".join(sql.readlines()))


def load_df_to_sqlite3_db(df):
    """Load the dataframe into the staging table

    Arguments:
        df dataframe -- Should be validated through previous steps
                        that it matches designated staging table

    Returns:
        [type] -- [description]
    """
    engine = sqlalchemy.create_engine(f'sqlite:///{DIR_PATH}'
                                      f'\{DATABASE_NAME}.sqlite3')
    df.to_sql(STG_TABLE_NAME, con=engine, if_exists='append',
              index_label='COUNTRY_IDNT')

    engine.dispose()
