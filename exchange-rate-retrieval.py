from functions import(validate_exchangerate_api_string,
                      exchangerates_api_dataframe,
                      build_exchangerates_api_string,
                      execute_sql_into_db,
                      load_df_to_sqlite3_db)


# Provide staring and ending date.
# If you only want one day, put the same value for both
start_date = '2019-01-01'
end_date = '2019-08-08'

# Change if you want a currency other than USD
# This is a 3 character country code!
base_currency = 'USD'
# If you want to limit the currencies that are returned,
# provide a list of comma seperated strings
comparisons = ['All']
# Indicates if we need to do a full refresh of all data
# False = data will be appended.
full_refresh = True

# This is where all the magic happens.
api_string = build_exchangerates_api_string(end_date, start_date,
                                            base_currency, comparisons)

if validate_exchangerate_api_string(api_string):
    exchangerates_df = exchangerates_api_dataframe(api_string)
    print(api_string)
    if full_refresh:
        execute_sql_into_db('drop')
    execute_sql_into_db('create')
    load_df_to_sqlite3_db(exchangerates_df)
else:
    print("You did not provide correct parameters. Please try again")
