# exhange-rate-data

## Project Details
- Original Author: Vishesh Shankar
- Update Author: 
- Update Date: 8-27-2019

## Project Purpose
- Retrieve data from API 
- Load data into SQLite DB
- Need to account for weekends not having data and will need to apply Friday's rate to missing dates
- Be able to reload database on demand

### How it Works//How to Run
- Using `exchange-rate-retrieval.py`:
    - Update arguements as noted at the top of the script.
    - Using these arguements:
        - A string will be built to interact with the Exchange Rates API
        - Extract data from API and use Pandas Dataframes to transform data into a normalized structure
        - If necessary, ability to drop and recreate database tables.
        - Load Dataframe into SQlite DB
- All data loaded into database, with weekends included, can be found in following view:
    `EXCHANGE_RATES_ALL_VW`
- View that shows How many CADs is required to buy $1 USD:
    `CAD_TO_1_USD_VW`

### Business/Tech Terms
- API: "Set of subroutine definitions, communication protocols, and tools for building software. In general terms,
        it is a set of clearly defined methods of communication among various components."
        https://en.wikipedia.org/wiki/Application_programming_interface
- SQLite DB: "SQLite is a C-language library that implements a small, fast, self-contained, high-reliability,
              full-featured, SQL database engine. SQLite is the most used database engine in the world."
              https://www.sqlite.org/index.html
- Pandas Dataframes: "Pandas DataFrame is nothing but an in-memory representation of an excel sheet via Python
                      programming language"
                      https://towardsdatascience.com/pandas-dataframe-a-lightweight-intro-680e3a212b96

## Getting Started
- Clone this Repo
- Use `pip install -r requirements.txt` to retrieve the necessary packages to run this project

### Latest Updates 
- Updated README to include how to Run file
- Cleaned up some erroneous code


### Work to Be done
- Create a check to determine whether data needs to be loaded or not.