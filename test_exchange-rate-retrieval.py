import pytest

from functions import(validate_exchangerate_api_string,
                      exchangerates_api_dataframe,
                      build_exchangerates_api_string)


class Test_exchangerates_api_Dates:

    def test_latest(self):
        # Removed the "Latest" API call due to the data being
        # returned in a different format.
        with pytest.raises(TypeError):
            build_exchangerates_api_string()

    def test_end_Date(self):
        assert ('https://api.exchangeratesapi.io/history?'
                'start_at=2019-08-23&end_at=2019-08-23') == \
                build_exchangerates_api_string(end_date='2019-08-23')

    def test_start_Date(self):
        # Required to provide a end date
        with pytest.raises(TypeError):
                build_exchangerates_api_string(start_date='2019-08-22')

    def test_start_end_Date(self):
        assert ('https://api.exchangeratesapi.io/history?'
                'start_at=2019-08-18&end_at=2019-08-23') == \
                build_exchangerates_api_string(start_date='2019-08-18',
                                               end_date='2019-08-23')


class Test_exchangerates_api_Base:
    @classmethod
    def setup_class(cls):
        cls.end_date = '2019-08-23'
        cls.start_date = '2019-08-22'

    def test_US_Currency(self):
        assert ('https://api.exchangeratesapi.io/history?'
                'start_at=2019-08-22&end_at=2019-08-23&base=US') == \
                build_exchangerates_api_string(start_date=self.start_date,
                                               end_date=self.end_date,
                                               base_currency='US')

    def test_EUR_Currency(self):
        assert ('https://api.exchangeratesapi.io/history?'
                'start_at=2019-08-22&end_at=2019-08-23') == \
                build_exchangerates_api_string(start_date=self.start_date,
                                               end_date=self.end_date,
                                               base_currency='EUR')


class Test_exchangerates_api_Symbols:
    @classmethod
    def setup_class(cls):
        cls.end_date = '2019-08-23'
        cls.start_date = '2019-08-22'
        cls.base_currency = 'US'

    def test_choose_symbols(self):
        assert ('https://api.exchangeratesapi.io/history?'
                'start_at=2019-08-22&end_at=2019-08-23&base=US'
                '&symbols=CA,EUR') == \
                build_exchangerates_api_string(start_date=self.start_date,
                                               end_date=self.end_date,
                                               base_currency=self.base_currency,
                                               comparisons=['CA', 'EUR'])
